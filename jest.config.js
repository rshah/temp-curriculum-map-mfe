module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  clearMocks: true,
  collectCoverage: true,
  coverageReporters: [
    "json",
    "lcov",
    "clover",
    "cobertura",
    "text"
  ],
  coverageDirectory: "coverage",
  coveragePathIgnorePatterns: [
    "/node_modules/",
    "/dist/"
  ],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
  testPathIgnorePatterns: [
    "/node_modules/",
    "/dist/"
  ],
  watchPathIgnorePatterns: [
    "/node_modules/",
    "/dist/"
  ],
  reporters: [
    "default",
  ],
};
