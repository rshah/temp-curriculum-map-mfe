# Welcome to MFE QuickStart application

Please refer to the official documentation in confluence:

- [General implementation path](https://powerschoolgroup.atlassian.net/wiki/spaces/INTDEVPORTAL/pages/64834045017/Dashboard+and+PowerTile+Micro+Frontends+MFEs+Technical+Implementation+Path).
- [Specific document on setting up this repo](https://powerschoolgroup.atlassian.net/wiki/spaces/INTDEVPORTAL/pages/64836108858/Solution+Layer+Path+Customize+the+QuickStart+Template)
