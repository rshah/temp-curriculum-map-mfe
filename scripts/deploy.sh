#!/bin/bash
set -e

update_account_env() {
    if [ $PS_ENVIRONMENT = 'latest' ]
        then
            sed -i '' "s/$2/$3/" $1
    fi
}

fileName='account-env.json'
placeholder='placeholder'
newValue='latest'

# Read account-env.json and replace name
update_account_env $fileName $placeholder $newValue

npm run deploy:cd

# Undo account-env.json change
update_account_env $fileName $newValue $placeholder

# Invalidate CloudFront cache
npm run cf-invalidate
# 
