const path = require('path');
const ModuleFederationPlugin = require('webpack').container.ModuleFederationPlugin;
const deps = require('./package.json').dependencies;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const neonVersion = require('./package.json').neonVersion;
const neonVersionUnderscored = neonVersion.replaceAll('.', '_');
const ESLintPlugin = require('eslint-webpack-plugin');


// Someday we might want the features listed here:https://github.com/module-federation/module-federation-examples/issues/566
// The ExternalTemplatesRemotePlugin takes care of those issues.
// const ExternalTemplatesRemotePlugin = require('external-remotes-plugin');

//Start using webpack using the command `npx webpack serve`
module.exports = (env) => {
  const mode = env.mode || 'development';
  const port = env.port || 8002;
  const neonServer = env.neonServer || 'https://assets.powerschool.com';
  const neonUnversionedServer = `${neonServer}/neon/unversioned`;
  const neonVersionedServer = `${neonServer}/neon/${neonVersion}`;

  // this needs to be unique across all of powerschool
  // https://powerschoolgroup.atlassian.net/wiki/spaces/INTDEVPORTAL/pages/64772735063/Micro-frontend+MFE+Catalog
  const mfeNameSpacePrefix = 'curriculum-map';
  const entry = mode === 'development' ? './app/bootstrap.tsx' : './hello-world/hello-world.tsx';

  return {
    mode,
    entry,
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist'),
      clean: true
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                  'ts-loader',
                  {
                    loader: 'string-replace-loader',
                    options: {
                      multiple: [{
                        search: '__neon__',
                        replace: 'neon-' + neonVersionUnderscored + '-',
                        flags: 'g'
                      }, {
                        search: '__NEON_CDN_URL__',
                        replace: neonVersionedServer,
                        flags: 'g'
                      }, {
                        search: '__NEON_CDN_UN_VERSIONED__',
                        replace: neonUnversionedServer,
                        flags: 'g'
                      }, {
                        search: '__mfe__',
                        replace: 'mfe-' + mfeNameSpacePrefix + '-',
                        flags: 'g'
                      }]
                    }
                  }
                ],
                exclude: /node_modules/
            },
            {
              test: /\.scss$/i,
              use: [
                // Creates `style` nodes from JS strings
                'style-loader',
                // Translates CSS into CommonJS
                'css-loader',
                {
                  loader: 'string-replace-loader',
                  options: {
                    multiple: [{
                      search: '__neon__',
                      replace: 'neon-' + neonVersionUnderscored + '-',
                      flags: 'g'
                    }, {
                      search: '__mfe__',
                      replace: 'mfe-' + mfeNameSpacePrefix + '-',
                      flags: 'g'
                    }]
                  }
                },
                // Compiles Sass to CSS
                'sass-loader',
              ]
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.tx', '.js', '.ts'],
      fallback: {
        https: false,

      },
    },
    devServer: {
        static: {
            directory: path.join(__dirname, 'dist')
        },
        port,
        historyApiFallback: true,
    },
    plugins: [
        //This one injects the javascript into the index.html page.
        new HtmlWebpackPlugin({
            template: './index.html'
        }),
        new ModuleFederationPlugin({
            //The name is going to be put on the global namespace of the browser. It is important to keep this unique across
            //PowerSchool to prevent namespace collisions. Please use the following pattern to name your MFE.
            //ps_mfe_{application-name}_{project-name or other information}

            name: 'curriculum_map', //TODO: We NEED to come up with a strategy for this because whatever shows up in "name" goes onto the window.
            filename: 'remoteEntry.js',
            //We can assign the remoteEntry to a specific variable on the window if we want to.
            // library: {
            //     name: 'window.powerSchoolFederatedModules.sis',
            //     type: 'assign'
            // },
            exposes: {
              './CurriculumMap': './hello-world/hello-world',
            },
        }),
        new ESLintPlugin({
          fix: true,
          extensions: ['.ts', 'tsx']
        }),
    ]
  };
};