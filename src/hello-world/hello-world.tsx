import React from 'react';
import { render } from 'react-dom';

export function HelloWorld(): JSX.Element {
  return (
    <div>
      <h1>Hello World!</h1>
    </div>
  );
}

export function init(element: HTMLElement | null): void {
  if (element) {
    render(<HelloWorld />, element);
  }
}
