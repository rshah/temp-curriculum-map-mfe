import React from 'react';
import ReactDOM from 'react-dom';
import { HelloWorld } from '../hello-world/hello-world';
import { BrowserRouter } from 'react-router-dom';

console.log(
  'This file is for running your MFE stand alone. Note: this file will not be rendered via module federation.'
);

ReactDOM.render(
  <BrowserRouter>
    <HelloWorld />
  </BrowserRouter>,
  document.getElementById('react-app-injection')
);
