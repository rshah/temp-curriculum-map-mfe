import {
  AccountEnvProps,
  cleanName,
  getAccountEnvPropsForName,
  getAllAccountEnvProps,
  getBranch
} from '@ps-refarch/cdk-utils';
import * as cdk from 'aws-cdk-lib';
import { readFileSync } from 'fs';
import { join } from 'path';
import { MainStack } from '../lib/main-stack';

const PS_ROOT_DIR = join(__dirname, '../../..');

/* eslint-disable no-new */

(async () => {
  // We're using the GitOps model and determining our envName from the (slightly cleaned-up)
  // branch name, unless it's overridden by PS_ENVIRONMENT.
  const envName = process.env.PS_ENVIRONMENT || cleanName(await getBranch());
  const acctEnvProps: AccountEnvProps = getAccountEnvPropsForName(
    await getAllAccountEnvProps(PS_ROOT_DIR),
    envName
  );

  // Find out the package name. This allows separate applications in the
  // same account.
  const packageName = JSON.parse(
    readFileSync(`${PS_ROOT_DIR}/package.json`).toString()
  ).name;
  const app = new cdk.App();
  new MainStack(app, `${cleanName(packageName)}-${envName}-stack`, {
    env: acctEnvProps.env,
    acctEnvProps,
    envName,
    tags: {
      environment: envName,
      mfe_id: packageName
    }
  });
})().catch(async (e) => {
  console.error(e); // eslint-disable-line no-console
  process.exit(1);
});
