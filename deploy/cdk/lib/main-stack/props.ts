import { AccountEnvProps } from '@ps-refarch/cdk-utils';
import { StackProps } from 'aws-cdk-lib';

export interface MainStackProps extends StackProps {
  /**
   * The environment name. This is either something like `dev`, `qa`, `prod`, etc. for
   * fixed deployed environments, or `dev/your-feature-branch-name` for feature branch
   * deployments.
   */
  envName: string;

  /**
   * The basic account environment. Taken from account-env.json.
   */
  acctEnvProps: AccountEnvProps;
}
