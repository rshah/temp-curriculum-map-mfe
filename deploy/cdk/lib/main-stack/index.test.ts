import {
  AccountEnvProps,
  addAssetSnapshotSerializer
} from '@ps-refarch/cdk-utils';
import * as cdk from 'aws-cdk-lib';
import { Template } from 'aws-cdk-lib/assertions';
import { MainStack } from './index';
import { MainStackProps } from './props';

describe('Application Stack', () => {
  // Bogus constants for testing
  const BOGUS_ACCOUNT_ENV_PROPS: AccountEnvProps = {
    names: ['bogus-dev'],
    env: { account: '999999999', region: 'aq-south-1' },
    hostedZoneName: 'bogus-zone-name',
    hostedZoneId: 'bogus-zone-id-6789',
    isProduction: false
  };

  test('Snapshot Test', async () => {
    const app = new cdk.App();
    const stackProps: MainStackProps = {
      envName: 'dev',
      env: BOGUS_ACCOUNT_ENV_PROPS.env,
      acctEnvProps: BOGUS_ACCOUNT_ENV_PROPS
    };
    const stack = new MainStack(app, 'mytest', stackProps);

    const template = Template.fromStack(stack);

    // Ignore asset stuff
    addAssetSnapshotSerializer(
      BOGUS_ACCOUNT_ENV_PROPS.env.account,
      BOGUS_ACCOUNT_ENV_PROPS.env.region
    );
    expect.addSnapshotSerializer({
      test: (val) =>
        typeof val === 'string' &&
        /FrontEndrewriterequest[0-9a-f]{32}[0-9A-F]{8}$/.test(val),
      print: () => {
        return `"[REWRITER HASH]"`;
      }
    });
    expect.addSnapshotSerializer({
      test: (val) =>
        typeof val === 'string' &&
        /FrontEndrewriter[0-9a-f]{24}[0-9A-F]{8}$/.test(val),
      print: () => {
        return `"[REWRITEREQ HASH]"`;
      }
    });

    // This will ensure that the configuration the stack does not have any unintended changes
    // Note: this automatically creates a snapshot if there isn't one; otherwise, it will compare
    //       the state of the template with the saved snapshot
    // eslint-disable-next-line max-len
    expect(template.toJSON()).toMatchSnapshot();
  });

  describe('Errors', () => {
    const app = new cdk.App();

    test('no hostedZoneId', () => {
      const bogusAccountEnvPropsCopy = {
        ...BOGUS_ACCOUNT_ENV_PROPS
      };

      // @ts-ignore
      delete bogusAccountEnvPropsCopy.hostedZoneId;

      const stackProps: MainStackProps = {
        envName: 'dev',
        env: BOGUS_ACCOUNT_ENV_PROPS.env,
        acctEnvProps: bogusAccountEnvPropsCopy
      };

      expect(() => new MainStack(app, 'mytest1', stackProps)).toThrowError(
        'requires hostedZoneId/hostedZoneName for environment dev'
      );
    });

    test('no hostedZoneName', () => {
      const bogusAccountEnvPropsCopy = {
        ...BOGUS_ACCOUNT_ENV_PROPS
      };

      // @ts-ignore
      delete bogusAccountEnvPropsCopy.hostedZoneName;

      const stackProps: MainStackProps = {
        envName: 'dev',
        env: BOGUS_ACCOUNT_ENV_PROPS.env,
        acctEnvProps: bogusAccountEnvPropsCopy
      };

      expect(() => new MainStack(app, 'mytest2', stackProps)).toThrowError(
        'requires hostedZoneId/hostedZoneName for environment dev'
      );
    });

    test('no env', () => {
      // @ts-ignore
      const stackProps: MainStackProps = {
        envName: 'dev',
        acctEnvProps: BOGUS_ACCOUNT_ENV_PROPS
      };

      expect(() => new MainStack(app, 'mytest3', stackProps)).toThrowError(
        'must pass fixed environment'
      );
    });
  });
});
