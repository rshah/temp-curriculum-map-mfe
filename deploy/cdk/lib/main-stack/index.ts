import { FRONTEND, FrontEnd, MFEDomain } from '@ps-refarch/cdk-utils';
import { CoreStack } from '@ps-refarch/serverless-constructs';
import { CfnOutput } from 'aws-cdk-lib';

import { Construct } from 'constructs';
import { join } from 'path';

import { MainStackProps } from './props';

/* eslint-disable no-new */
export class MainStack extends CoreStack {
  public constructor(
    scope: Construct,
    id: string,
    appStackProps: MainStackProps
  ) {
    super(scope, id, appStackProps);

    if (appStackProps.env == null) {
      throw new Error('must pass fixed environment');
    }

    const { acctEnvProps } = appStackProps;
    const { envName } = appStackProps;

    // Check required fields in acct env
    if (!acctEnvProps.hostedZoneId || !acctEnvProps.hostedZoneName) {
      throw new Error(
        `requires hostedZoneId/hostedZoneName for environment ${envName}`
      ); // eslint-disable-line no-console
    }

    // Build everything that has to do with the MFE's domain.
    const domain = new MFEDomain(this, 'MFE domain', {
      envName,
      region: acctEnvProps.env.region,
      hostedZoneId: acctEnvProps.hostedZoneId,
      hostedZoneName: acctEnvProps.hostedZoneName,
      isProduction: acctEnvProps.isProduction
    });

    // Build the front end
    new FrontEnd(this, 'Front End', {
      domain,
      envName,
      webRoot: join(__dirname, '../../../../src/dist'),
      isProduction: acctEnvProps.isProduction
    });

    new CfnOutput(this, 'Front End DNS', {
      value: domain.getDomainName(FRONTEND)
    });
  }
}
